package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {


    String defend;
    String type;

    public DefendWithArmor(){
        this.defend = "dragons blood";
        this.type = "shivas guard";
    }

    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
