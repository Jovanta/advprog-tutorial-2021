package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    String attack;
    String type;

    public AttackWithGun(){
        this.attack = "fire";
        this.type = "glock";
    }


    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
