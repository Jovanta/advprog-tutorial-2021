package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {


    String attack;
    String type;

    public AttackWithSword(){
        this.attack = "greater cleave";
        this.type = "crystalis";
    }

    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
