package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {



    String defend;
    String type;

    public DefendWithShield(){
        this.defend = "apothic shield";
        this.type = "assault cuirass";
    }
    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
