package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {



    String attack;
    String type;

    public AttackWithMagic(){
        this.attack = "telekinesis";
        this.type = "aghanim scepter";
    }

    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
