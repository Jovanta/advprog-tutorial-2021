package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {



    String defend;
    String type;

    public DefendWithBarrier(){
        this.defend = "fissure";
        this.type = "solar crest";
    }
    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
    //ToDo: Complete me
}
